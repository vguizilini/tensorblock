## TensorBlock  - Modular API for Tensorflow ##
### by Vitor Campanholo Guizilini ###

------------------------------------------------------------------

Tensorblock is a Tensorflow API for fast and intuitive development, that aims to encapsulate everything you add to a graph into a single class, that can be easily accessed and modified.
The goal is to isolate each module as much as possible, so new features can be implemented with minimal changes to the rest of the codebase.

Tensorblock is still a work in progress, and right now it has support to the following:

## Layers 

- Fully Connected
- Variational
- 2D/3D Convolution
- 2D Deconvolution
- Static/Dynamic RNNs
- Miscellaneous: Pooling / Dropout / Activations / etc...

Examples of the applications of these layers in well-known architectures can be found in /demos. These currently are:

- 2D MNIST (Convolutional / Static RNN) Classification
- 3D ModelNet10 (Convolutional) Classification
- 2D MNIST (Variational) (Convolutional) Auto-Encoder

## Tensorboard 

- Automatic generation of variable and name scopes, aiming to make easy to read graphs.
- Creation of blocks that contain specific parts of the graph, again aiming to make it easier to read.
- Intuitive copying and sharing of variables (or even blocks), both during initialization and as operations.
- Easy generation of summaries and plots, to track variables during the training process.

## Reinforcement Learning 

Tensorblock also features an interface for the creation of Deep Reinforcement Learning techniques and their deployment against different games with minimal changes.
Two base classes, source and player, are provided, the former dealing with game data and the latter dealing with the AI that will be playing the game. New classes can then be derived from these two and directly access all relevant data, having only to implement strictly necessary methods.

Right now it supports the following game libraries:

- Pygame ( it comes with a self-made simple game, called "Catch" )
- OpenAI Gym

And the following Deep Reinforcement Learning techniques:

- Basic Random Player
- e-Greedy Deep-Q-Learning Player

For a demo of a trained model (around 1 hour), go to /reinforcement and run ./script_test.py.

## Contributions 

Contributions and feedback are more than welcome!
