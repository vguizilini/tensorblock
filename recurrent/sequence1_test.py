
import sys
sys.path.append( '..' )
import tensorblock as tb
import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

data = tb.aux.load_list( 'data/test_data1' )

max_seqlen = 199

recipe = tb.recipe()

recipe.addInput( shape = [ None , max_seqlen , 1 ] , name = 'Input' )
recipe.addInput( shape = [ None , max_seqlen , 1 ] , name = 'Label' )
recipe.addInput( shape = [ None ] , name = 'SeqLen' , dtype = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , name = 'RNN' ,
                 seqlen = 'SeqLen' , num_cells = 2 , out_dropout = 0.8 , out_channels = 1 ,
                 activation = None )

recipe.addOperation( function = tb.ops.masked_mean_squared_error ,
                     input = [ 'Label' , 'SeqLen' ] , extra = 'RNN' , name = 'Cost' )
recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSaver( name = 'Saver' , dir = 'trained_sequence1a' )

recipe.initialize()
recipe.restore( name = 'Saver' )

n = 0

seq , seqlen = [] , 0
for i in range( max_seqlen ):
    seq.append( [ -2.0 ] )

p = [ data[n][0][0] ]
for i in range( len( data[n] ) ):

    seqlen += 1
    seq[i] = data[n][i]

    dict = [ [ 'Input'  , [ seq    ] ] ,
             [ 'SeqLen' , [ seqlen ] ] ]

    val = recipe.run( 'RNN' , dict , use_dropout = False )
    p.append( val[0][0] )

full = recipe.run( recipe.info( 'RNN' )[0] , dict , use_dropout = False )

x = np.arange( 0 , len( data[n] ) )

y = []
for i in range( len( x ) ):
    y.append( seq[i][0] )

f = [ data[n][0][0] ]
for i in range( len( x ) ):
    f.append( full[0][i][0] )

p = p[:-1]
f = f[:-1]

plt.plot( x , y )
plt.plot( x , p )
plt.plot( x , f )
plt.show()
