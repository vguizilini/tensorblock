
import sys
sys.path.append( '..' )
import tensorblock as tb

import random
import numpy as np

fwd = 10

data , labels = [] , []
for i in range( 20000 ):

    qty = 100 + random.randint( 0 , 100 )

    ampl = 0.1 + random.random() / 10
    freq = 0.1 + random.random() / 10

    data.append( [] )
    for j in range( 0 , qty ):
        data[i].append( [ ampl * np.sin( j * freq ) ] )

    labels.append( [] )
    for j in range( 0 , qty - fwd ):

        pred = [ data[i][j + k][0] for k in range( fwd ) ]
        labels[i].append( pred )

    data[i] = data[i][0:-fwd]

prop = int( 9 * len( data ) / 10 )
train_data , train_labels = data[ :prop ] , labels[ :prop ]
test_data  , test_labels  = data[ prop: ] , labels[ prop: ]

tb.aux.save_list( 'data/train_data2' , train_data )
tb.aux.save_list( 'data/test_data2' , test_data )
tb.aux.save_list( 'data/train_labels2' , train_labels )
tb.aux.save_list( 'data/test_labels2' , test_labels )
