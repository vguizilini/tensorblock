
import sys
sys.path.append( '../..' )
import tensorblock as tb
import tensorflow as tf

import numpy as np
import random

train_data   = tb.aux.load_list( 'train_data'   )
train_labels = tb.aux.load_list( 'train_labels' )
test_data    = tb.aux.load_list( 'test_data'    )
test_labels  = tb.aux.load_list( 'test_labels'  )

max_seqlen = 20
n_hidden , n_classes = 64 , 2

train_seqlen = tb.aux.pad_data( train_data , max_seqlen )
test_seqlen = tb.aux.pad_data( test_data , max_seqlen )

recipe = tb.recipe()

x = recipe.addInput( shape = [ None , max_seqlen , 1 ] , name = 'Input' , type = tf.float32 )
y = recipe.addInput( shape = [ None , n_classes ] , name = 'Label' , type = tf.float32 )
seqlen = recipe.addInput( shape = [ None ] , name = 'SeqLen' , type = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , seqlen = 'SeqLen' ,
                 in_dropout = 0.9 , out_dropout = 0.9 ,
                 out_channels = n_hidden , name = 'LSTM' )

recipe.addLayer( type = tb.layers.fully , input = 'LSTM' ,
                 out_channels = n_classes , name = 'Output' , activation = None ,
                 weight_type = tb.vars.random_normal , weight_stddev = 1.0 ,
                 bias_type = tb.vars.random_normal , bias_stddev = 1.0 )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Eval' )

recipe.addOperation( function = tb.optims.adam , learning_rate = 0.01 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSummaryScalar( input = [ 'Cost' , 'Eval' ] )
recipe.addSummary( name = 'Summary' )
recipe.addWriter( name = 'Writer' , dir = 'logs/tests' )

recipe.initialize()
recipe.printNodes()
recipe.printCollection()

recipe.train(
        train_data = train_data , train_labels = train_labels , train_seqlen = train_seqlen ,
        test_data = test_data , test_labels = test_labels , test_seqlen = test_seqlen ,
        optimizer = 'Optimizer' ,
        eval_function = [ 'Cost' , 'Eval' ] , eval_freq = 1 ,
        size_batch = 500 , num_epochs = 10000 )





