
import sys
sys.path.append( '../..' )
import tensorblock as tb
import tensorflow as tf
import numpy as np
import random
import time

random.seed( 1 )
class ToySequenceData(object):

    def __init__( self , n_samples = 1000 , max_seq_len = 20 , min_seq_len = 3 ,
                  max_value = 1000 ):

        self.data = []
        self.labels = []
        self.seqlen = []

        for i in range(n_samples):

            len = random.randint( min_seq_len , max_seq_len )
            self.seqlen.append( len )

            if random.random() < 0.5:

                rand_start = random.randint( 0 , max_value - len )

                s = [ [ float(i) / max_value ] for i in range( rand_start , rand_start + len ) ]

                s += [ [ 0.0 ] for i in range( max_seq_len - len ) ]
                self.data.append( s )
                self.labels.append( [ 1.0 , 0.0 ] )
            else:

                s = [ [ float( random.randint( 0 , max_value ) ) / max_value ] for i in range( len ) ]
                s += [ [ 0.0 ] for i in range( max_seq_len - len ) ]

                self.data.append( s )
                self.labels.append( [ 0.0 , 1.0 ] )

        self.batch_id = 0

    def next(self, batch_size):

        if self.batch_id == len( self.data ):
            self.batch_id = 0

        batch_data   = ( self.data[   self.batch_id : min( self.batch_id + batch_size , len( self.data ) ) ] )
        batch_labels = ( self.labels[ self.batch_id : min( self.batch_id + batch_size , len( self.data ) ) ] )
        batch_seqlen = ( self.seqlen[ self.batch_id : min( self.batch_id + batch_size , len( self.data ) ) ] )

        self.batch_id = min( self.batch_id + batch_size , len( self.data ) )

        return batch_data , batch_labels , batch_seqlen





learning_rate = 0.01
training_iters = 1000000
batch_size = 128
display_step = 10

seq_max_len = 20
n_hidden = 64
n_classes = 2

recipe = tb.recipe()

trainset = ToySequenceData( n_samples = 1000 , max_seq_len = seq_max_len )
testset  = ToySequenceData( n_samples =  500 , max_seq_len = seq_max_len )

train_data   = np.array( trainset.data )
train_labels = np.array( trainset.labels )
train_seqlen = np.array( trainset.seqlen )

test_data   = np.array( testset.data )
test_labels = np.array( testset.labels )
test_seqlen = np.array( testset.seqlen )

print( train_data.shape )
print( train_labels.shape )
print( train_seqlen.shape )

x = recipe.addInput( shape = [ None , seq_max_len , 1 ] , name = 'Input' , type = tf.float32 )
y = recipe.addInput( shape = [ None , n_classes ] , name = 'Label' , type = tf.float32 )
seqlen = recipe.addInput( shape = [ None ] , name = 'SeqLen' , type = tf.int32 )

recipe.addLayer( type = tb.layers.lstm , input = 'Input' , seqlen = 'SeqLen' ,
                 out_channels = n_hidden , name = 'LSTM' , activation = None )

recipe.addLayer( type = tb.layers.fully , input = 'LSTM' ,
                 out_channels = n_classes , name = 'Output' , activation = None ,
                 weight_type = tb.vars.random_normal , weight_stddev = 1.0 , weight_seed = 1 ,
                 bias_type = tb.vars.random_normal , bias_stddev = 1.0 , bias_seed = 1 )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Eval' )

recipe.addOperation( function = tb.optims.gradient_descent , learning_rate = learning_rate ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.initialize()

print( recipe.eval( 'W_Output' ) )
print( recipe.eval( 'b_Output' ) )

recipe.train(
        train_data = train_data ,
        train_labels = train_labels ,
        train_seqlen = train_seqlen ,
        test_data = test_data ,
        test_labels = test_labels ,
        test_seqlen = test_seqlen ,
        optimizer = 'Optimizer' ,
        eval_function = [ 'Cost' , 'Eval' ] , eval_freq = 1 ,
        size_batch = 500 , num_epochs = 5000 )



#step = 1
#while step * batch_size < training_iters:

#    batch_x , batch_y , batch_seqlen = trainset.next( batch_size )

#    dict = [ [ 'Input'  , batch_x      ] ,
#             [ 'Label'  , batch_y      ] ,
#             [ 'SeqLen' , batch_seqlen ] ]

#    recipe.run( 'Optimizer' , [ [ 'Input' , batch_x ] ,
#                                [ 'Label' , batch_y ] ,
#                                [ 'SeqLen' , batch_seqlen ] ] )

#    if step % display_step == 0:

#        acc , loss = recipe.run( [ 'Eval' , 'Cost' ] , dict )
#        print("Iter " + str(step*batch_size) + ", Minibatch Loss= " + \
#              "{:.6f}".format(loss) + ", Training Accuracy= " + \
#              "{:.5f}".format(acc))

#    step += 1

#dict = [ [ 'Input'  , testset.data   ] ,
#         [ 'Label'  , testset.labels ] ,
#         [ 'SeqLen' , testset.seqlen ] ]

#print("Testing Accuracy:", recipe.run( 'Eval' , dict ) )


#class Dataset:

#    train_data , test_data = [] , []
#    train_labels , test_labels = [] , []
#    train_seqlen , test_seqlen = [] , []

#    def __init__( self , n_train , n_test ):

#        data = []
#        labels = []
#        seqlen = []

#        max_size = 20

#        for _ in range( n_train + n_test ):

#            rnd = random.randint( 0 , 1 )

#            seed = random.randint( 0 , 100 )
#            size = random.randint( 10 , max_size - 1 )
##            size = max_size - 1

#            sample = []
#            sample.append( seed )

#            for _ in range( size ):
#                if rnd == 0: sample.append( sample[-1] + 1 )
#                if rnd == 1: sample.append( sample[-1] + 2 )
#            sample += [ None for i in range( max_size - size - 1 ) ]

#            data.append( [ sample ] )
#            seqlen.append( size + 1 )

#            label = [ 0 , 0 ] ; label[ rnd ] = 1
#            labels.append( label )

#        self.train_data   = np.array(   data[ :n_train ] )
#        self.train_labels = np.array( labels[ :n_train ] )
#        self.train_seqlen = np.array( seqlen[ :n_train ] )

#        self.test_data   = np.array(   data[ n_train: ] )
#        self.test_labels = np.array( labels[ n_train: ] )
#        self.test_seqlen = np.array( seqlen[ n_train: ] )

#        self.train_data = np.transpose( self.train_data , [ 0 , 2 , 1 ] )
#        self.test_data  = np.transpose( self.test_data  , [ 0 , 2 , 1 ] )

#dataset = Dataset( 10000 , 1000 )

#print( dataset.test_data[0:5,:] )
#print( dataset.test_seqlen[0:5] )

#recipe = tb.recipe()

#recipe.addInput( shape = dataset.train_data.shape   , name = 'Input'  )
#recipe.addInput( shape = dataset.train_labels.shape , name = 'Label'  )
#recipe.addInput( shape = [ None ] , type = tf.int32 , name = 'SeqLen' )

#recipe.addLayer( type = tb.layers.lstm , input = 'Input' , seqlen = 'SeqLen' ,
#                 out_channels = 128 , name = 'LSTM' , activation = None )

#recipe.addLayer( type = tb.layers.fully , input = 'LSTM' ,
#                 out_channels = 'Label' , name = 'Output' , activation = None )

#recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
#                     input = [ 'Output' , 'Label' ] , name = 'Cost' )
#recipe.addOperation( function = tb.ops.mean_equal_argmax ,
#                     input = [ 'Output' , 'Label' ] , name = 'Eval' )

#recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
#                     input = 'Cost' , name = 'Optimizer' )

#recipe.initialize()

#recipe.printNodes()
#recipe.printCollection()

##print( recipe.info( 'LSTM' ) )

#print( dataset.test_seqlen )
#print( recipe.run( recipe.info( 'LSTM' )[0] , [ [ 'Input' , dataset.test_data     ] ,
#                                                [ 'SeqLen' , dataset.test_seqlen  ] ] ) )
##print( res[0] )
##print( res.shape )

#recipe.train(
#        train_data = dataset.train_data ,
#        train_labels = dataset.train_labels ,
#        train_seqlen = dataset.train_seqlen ,
#        test_data = dataset.test_data ,
#        test_labels = dataset.test_labels ,
#        test_seqlen = dataset.test_seqlen ,
#        optimizer = 'Optimizer' ,
#        eval_function = [ 'Cost' , 'Eval' ] , eval_freq = 1 ,
#        size_batch = 100 , num_epochs = 500 )
