
import sys
sys.path.append( '..' )
import tensorblock as tb
import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt

data = tb.aux.load_list( 'data/test_data2' )

max_seqlen = 199

recipe = tb.recipe()

recipe.addInput( shape = [ None , max_seqlen ,  1 ] , name = 'Input' )
recipe.addInput( shape = [ None , max_seqlen , 10 ] , name = 'Label' )
recipe.addInput( shape = [ None                   ] , name = 'SeqLen' , dtype = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , name = 'RNN' ,
                 seqlen = 'SeqLen' , num_cells = 2 , out_dropout = 0.8 , out_channels = 10 ,
                 activation = None )

recipe.addOperation( function = tb.ops.masked_mean_squared_error ,
                     input = [ 'Label' , 'SeqLen' ] , extra = 'RNN' , name = 'Cost' )

recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSaver( name = 'Saver' , dir = 'trained_sequence2a' )

recipe.initialize()
recipe.restore( name = 'Saver' )
recipe.printNodes()

n = 223

seq , seqlen = [] , 0
for i in range( max_seqlen ):
    seq.append( [ 0.0 ] )

xy = np.arange( 0 , len( data[n] ) )
y = data[n]

p = [ data[n][0][0] ]
for i in range( len( data[n] ) ):

    seqlen += 1
    seq[i] = data[n][i]

    dict = [ [ 'Input'  , [ seq    ] ] ,
             [ 'SeqLen' , [ seqlen ] ] ]

    val = recipe.run( 'RNN' , dict , use_dropout = False )

    p = []
    for i in range( seqlen ): p.append( seq[i][0] )
    for v in val[0]: p.append( v )
    xp = np.arange( 0 , len( p ) )

    pbx = xp[:seqlen]
    pby =  p[:seqlen]
    pax = xp[seqlen-1:]
    pay =  p[seqlen-1:]

    plt.clf()
	
    plt.plot( xy , y )
    plt.plot( pbx , pby )
    plt.plot( pax , pay )

    plt.pause(0.05)

#full = recipe.run( recipe.info( 'RNN' )[0] , dict , use_dropout = False )



#f = [ data[n][0][0] ]
#for i in range( len( x ) ):
#    f.append( full[0][i][0] )


#plt.plot( x , y )
#plt.plot( x , p )
#plt.plot( x , f )
#plt.show()
