
import sys
sys.path.append( '..' )
import tensorblock as tb

import random
import numpy as np

data , labels = [] , []
for i in range( 20000 ):

    qty = 100 + random.randint( 0 , 100 )

    ampl = 0.1 + random.random() / 10
    freq = 0.1 + random.random() / 10

    data.append( [] )
    for j in range( 0 , qty ):
        data[i].append( [ ampl * np.sin( j * freq ) ] )

    labels.append( [] )
    for j in range( 0 , qty - 1 ):
        labels[i].append( data[i][j + 1] )

    data[i] = data[i][0:-1]

prop = int( 9 * len( data ) / 10 )
train_data , train_labels = data[ :prop ] , labels[ :prop ]
test_data  , test_labels  = data[ prop: ] , labels[ prop: ]

tb.aux.save_list( 'data/train_data' , train_data )
tb.aux.save_list( 'data/test_data' , test_data )
tb.aux.save_list( 'data/train_labels' , train_labels )
tb.aux.save_list( 'data/test_labels' , test_labels )
