
import sys
sys.path.append( '../..' )
import tensorblock as tb
import tensorflow as tf
import numpy as np
import random

train_data = np.load(   'data/train_table_data.npy'   )
train_labels = np.load( 'data/train_table_labels.npy' )
train_seqlen = np.load( 'data/train_table_seqlen.npy' )

test_data = np.load(   'data/test_data_rnn5.npy'   )
test_labels = np.load( 'data/test_labels_rnn5.npy' )
test_seqlen = np.load( 'data/test_seqlen_rnn5.npy' )

NUM_SAMPLES , MAX_SEQLEN , DATA_DIM = train_data.shape
NUM_CLASSES = int( train_labels.shape[-1] )

NUM_EPOCHS    = 100000
NUM_TIMESTEPS = 5
SIZE_BATCH    = 100

NUM_TEST = int( np.sum( test_seqlen ) - test_data.shape[0] * NUM_TIMESTEPS )

recipe = tb.recipe()

recipe.addInput( shape = [ None , NUM_TIMESTEPS , DATA_DIM ] , name = 'Input' )
recipe.addInput( shape = [ None , NUM_CLASSES              ] , name = 'Label' )
recipe.addInput( shape = [ None                            ] , name = 'SeqLen' , dtype = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , name = 'RNN' ,
                 seqlen = 'SeqLen' , num_cells = 4 , cell_type = 'GRU' , out_channels = 300 ,
                 activation = tb.activs.relu )

recipe.addLayer( type = tb.layers.fully , name = 'Output' ,
                 dropout = 0.8 , activation = None , out_channels = NUM_CLASSES )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )

recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Eval' )

recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSaver( name = 'Saver' , dir = 'trained_gru' )
recipe.initialize()
recipe.restore( name = 'Saver' )
recipe.printNodes()

dict_test = [ [ 'Input'  , test_data   ] ,
              [ 'Label'  , test_labels ] ,
              [ 'SeqLen' , test_seqlen ] ]

output = recipe.run( 'Output' , dict_test , use_dropout = False )

out_est = np.argmax( output , axis = 1 )
out_lbl = np.argmax( test_labels , axis = 1 )

conf = np.zeros( [ 12 , 12 ] )

for i in range( out_est.shape[0] ):
    conf[ out_est[i] , out_lbl[i] ] += 1

print( conf )





