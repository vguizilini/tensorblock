
import sys
sys.path.append( '../..' )
import tensorblock as tb
import tensorflow as tf
import numpy as np
import random

train_data = np.load(   'data/train_table_data.npy'   )
train_labels = np.load( 'data/train_table_labels.npy' )
train_seqlen = np.load( 'data/train_table_seqlen.npy' )

test_data   = np.load( 'data/test_data_rnn5.npy'   )
test_labels = np.load( 'data/test_labels_rnn5.npy' )
test_seqlen = np.load( 'data/test_seqlen_rnn5.npy' )

NUM_SAMPLES , MAX_SEQLEN , DATA_DIM = train_data.shape
NUM_CLASSES = int( train_labels.shape[-1] )

NUM_EPOCHS    = 100000
NUM_TIMESTEPS = 5
SIZE_BATCH    = 100

NUM_TEST = int( np.sum( test_seqlen ) - test_data.shape[0] * NUM_TIMESTEPS )

dict_test = [ [ 'Input'  , test_data   ] ,
              [ 'Label'  , test_labels ] ,
              [ 'SeqLen' , test_seqlen ] ]

recipe = tb.recipe()

recipe.addInput( shape = [ None , NUM_TIMESTEPS , DATA_DIM ] , name = 'Input' )
recipe.addInput( shape = [ None , NUM_CLASSES              ] , name = 'Label' )
recipe.addInput( shape = [ None                            ] , name = 'SeqLen' , dtype = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , name = 'RNN' ,
                 seqlen = 'SeqLen' , num_cells = 4 , cell_type = 'GRU' , out_channels = 300 ,
                 activation = tb.activs.relu )

recipe.addLayer( type = tb.layers.fully , name = 'Output' ,
                 dropout = 0.8 , activation = None , out_channels = NUM_CLASSES )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )

recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Eval' )

recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSaver( name = 'Saver' , dir = 'trained_gru' )
recipe.initialize()
recipe.restore( name = 'Saver' )
recipe.printNodes()

for epoch in range( NUM_EPOCHS ):

    batch_data   = np.zeros( ( SIZE_BATCH , NUM_TIMESTEPS , DATA_DIM ) )
    batch_labels = np.zeros( ( SIZE_BATCH , NUM_CLASSES              ) )
    batch_seqlen = np.zeros( ( SIZE_BATCH                            ) )

    for i in range( SIZE_BATCH ):

        rnd_i = random.randint( 0 , NUM_SAMPLES - 1 )
        rnd_j = random.randint( 0 , train_seqlen[rnd_i] - NUM_TIMESTEPS )

        for j in range( NUM_TIMESTEPS ):
            batch_data[ i , j ] = train_data[ rnd_i , rnd_j + j ]
        batch_labels[ i ] = train_labels[ rnd_i , rnd_j + NUM_TIMESTEPS - 1 ]
        batch_seqlen[ i ] = NUM_TIMESTEPS

    dict_train = [ [ 'Input'  , batch_data    ] ,
                   [ 'Label'  , batch_labels  ] ,
                   [ 'SeqLen' , batch_seqlen  ] ]

    recipe.run( 'Optimizer' , dict_train , use_dropout = True )

    if epoch % 10 == 0:

        cost = recipe.run( 'Cost' , dict_train , use_dropout = True )
        print( '**** TRAIN COST : ' , cost )

    if epoch % 100 == 0:

        eval = recipe.run( 'Eval' , dict_test , use_dropout = False )
        print( '**** TEST ACCURACY : ' , eval )

#        recipe.save( name = 'Saver' )






