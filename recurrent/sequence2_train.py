
import sys
sys.path.append( '..' )
import tensorblock as tb
import tensorflow as tf
import numpy as np

train_data   = tb.aux.load_list( 'data/train_data2'   )
train_labels = tb.aux.load_list( 'data/train_labels2' )
test_data    = tb.aux.load_list( 'data/test_data2'    )
test_labels  = tb.aux.load_list( 'data/test_labels2'  )

max_seqlen = 199

train_seqlen = tb.aux.pad_data( train_data , max_seqlen )
test_seqlen  = tb.aux.pad_data( test_data , max_seqlen )

tb.aux.pad_data( train_labels , max_seqlen )
tb.aux.pad_data( test_labels  , max_seqlen )

recipe = tb.recipe()

recipe.addInput( shape = [ None , max_seqlen ,  1 ] , name = 'Input' )
recipe.addInput( shape = [ None , max_seqlen , 10 ] , name = 'Label' )
recipe.addInput( shape = [ None                   ] , name = 'SeqLen' , dtype = tf.int32 )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' , name = 'RNN' ,
                 seqlen = 'SeqLen' , num_cells = 2 , out_dropout = 0.8 , out_channels = 10 ,
                 activation = None )

recipe.addOperation( function = tb.ops.masked_mean_squared_error ,
                     input = [ 'Label' , 'SeqLen' ] , extra = 'RNN' , name = 'Cost' )

recipe.addOperation( function = tb.optims.adam , learning_rate = 0.001 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSaver( name = 'Saver' , dir = 'trained_sequence2a' )

recipe.initialize()
recipe.restore( name = 'Saver' )
recipe.printNodes()

dict_test = [ [ 'Input'  , test_data    ] ,
              [ 'Label'  , test_labels  ] ,
              [ 'SeqLen' , test_seqlen  ] ]

cost = recipe.run( 'Cost' , dict_test , use_dropout = False )
print( 'Epoch:' , 0 , 'Cost:' , cost )

num_epochs = 100
size_batch = 500

num_samples = len( train_data )
num_batches = int( num_samples / size_batch ) + 1

for epoch in range( num_epochs ):

    for batch in range( num_batches ):

        batch_data   = tb.aux.get_batch( train_data   , size_batch , batch )
        batch_labels = tb.aux.get_batch( train_labels , size_batch , batch )
        batch_seqlen = tb.aux.get_batch( train_seqlen , size_batch , batch )

        dict_train = [ [ 'Input'  , batch_data    ] ,
                       [ 'Label'  , batch_labels  ] ,
                       [ 'SeqLen' , batch_seqlen  ] ]

        recipe.run( 'Optimizer' , dict_train , use_dropout = True )

    cost = recipe.run( 'Cost' , dict_test , use_dropout = False )
    print( 'Epoch:' , epoch + 1 , 'Cost:' , cost )

    recipe.save( name = 'Saver' )
























