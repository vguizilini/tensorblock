
import tensorflow as tf

### Adam Optimizer
def adam( tensors , extras , pars ):

    return tf.train.AdamOptimizer( pars['learning_rate'] ).minimize( tensors[0] )

### Gradient Descent Optimizer
def gradient_descent( tensors , extras , pars ):

    return tf.train.GradientDescentOptimizer( pars['learning_rate'] ).minimize( tensors[0] )

