
import tensorflow as tf
import tensorblock as tb

### Copy
def copy( tensors , extras , pars ):

    list = []
    for i in range( len( tensors[0] ) ):
        list.append( tensors[1][i].assign( tensors[0][i] ) )
    return list

### Mean SoftMax Cross Entropy Logit
def mean_soft_cross_logit( tensors , extras , pars ):

    return tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(
                logits = tensors[0] , labels = tensors[1] ) )

### Weighted Mean SoftMax Cross Entropy Logit
def weighted_mean_soft_cross_logit( tensors , extras , pars ):

    return tf.reduce_mean( tf.multiply(
            tf.nn.softmax_cross_entropy_with_logits( tensors[0] , tensors[1] ) , tensors[2] ) )

### Mean Squared Error
def mean_squared_error( tensors , extras , pars ):

    return tf.reduce_mean( tf.square( tensors[0] - tensors[1] ) )

### Masked Mean Squared Error
def masked_mean_squared_error( tensors , extras , pars ):

    shape = tensors[0].get_shape().as_list()
    label , max_seqlen = tensors[0] , shape[1]

    if len( tensors ) == 3:
        output , seqlen = tensors[1] , tensors[2]
    else:
        output , seqlen = extras[0] , tensors[1]

    mask = tf.sequence_mask( seqlen , max_seqlen , dtype = tf.float32 )

    cost = tf.square( label - output )

    if len( shape ) == 3:
        cost = tf.reduce_sum( cost , reduction_indices = 2 )

    cost = tf.reduce_sum( cost * mask , reduction_indices = 1 )
    cost /= tf.reduce_sum( mask , reduction_indices = 1 )

    return tf.reduce_mean( cost )

### Mean Equal Argmax
def mean_equal_argmax( tensors , extras , pars ):

    correct = tf.equal( tf.argmax( tensors[0] , 1 , name = 'ArgMax_1' ) ,
                        tf.argmax( tensors[1] , 1 , name = 'ArgMax_2' ) )

    return tf.reduce_mean( tf.cast( correct , tf.float32 ) )

### Mean Cast
def mean_cast( tensors , extras , pars ):

    return tf.reduce_mean( tf.cast( tensors[0] , tf.float32 ) )

### Sum Mul
def sum_mul( tensors , extras , pars ):

    axis = len( tb.aux.tf_shape( tensors[0] ) ) - 1

    return tf.reduce_sum( tf.multiply(
                tensors[0] , tensors[1] ) , axis = axis )

### Mean Variational
def mean_variational( tensors , extras , pars ):

    z_mu , z_sig = extras
    z_mu2 , z_sig2 = tf.square( z_mu ) , tf.square( z_sig )

    rec_loss = tf.reduce_sum( tf.square( tensors[0] - tensors[1] ) )
    kl_div = - 0.5 * tf.reduce_sum( 1.0 + tf.log( z_sig2 + 1e-10 ) - z_mu2 - z_sig2 , 1 )

    return tf.reduce_mean( rec_loss + kl_div )
