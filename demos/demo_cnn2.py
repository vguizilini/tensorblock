
import sys
sys.path.append( '..' )
import tensorblock as tb

train_data   = tb.aux.load_numpy( './data/mnist_train_images' )
train_labels = tb.aux.load_numpy( './data/mnist_train_labels' )
test_data    = tb.aux.load_numpy( './data/mnist_test_images'  )
test_labels  = tb.aux.load_numpy( './data/mnist_test_labels'  )

recipe = tb.recipe()

recipe.addInput( shape = train_labels.shape , name = 'Label' )
recipe.addInput( shape = train_data.shape   , name = 'Input' )

out_conv2d_channels = [ 64 , 128 ]
out_fully_channels  = [ 1024 ]

recipe.setLayerDefaults( type = tb.layers.conv2d ,
                         ksize = 5 , pooling = 2 , activation = tb.activs.relu )

for n in out_conv2d_channels:
    recipe.addLayer( out_channels = n )

recipe.setLayerDefaults( type = tb.layers.fully ,
                         activation = tb.activs.relu )

for n in out_fully_channels:
    recipe.addLayer( out_channels = n , dropout = 0.5 )

recipe.addLayer( out_channels = 'Label' , activation = None ,
                 name = "Output" )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Evaluation' )
recipe.addOperation( function = tb.optims.adam ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addSummaryScalar( input = [ 'Cost' , 'Evaluation' ] )
recipe.addSummary( name = 'Summary' )
recipe.addWriter( name = 'Writer' , dir = 'logs/cnn2' )

recipe.initialize()
recipe.printNodes()

recipe.train(
        train_data = train_data , train_labels = train_labels ,
        test_data = test_data , test_labels = test_labels , test_length = 1000 ,
        optimizer = 'Optimizer' ,  summary = 'Summary' , writer = 'Writer' ,
        eval_function = [ 'Cost' , 'Evaluation' ] , eval_freq = 1 ,
        size_batch = 1000 , num_epochs = 500 )
