
import sys
sys.path.append( '..' )
import tensorblock as tb

train_data = tb.aux.load_numpy( './data/mnist_train_images' )
test_data  = tb.aux.load_numpy( './data/mnist_test_images'  )

channels = [ 32 , 64 , 128 ]
latent_size = 50

recipe = tb.recipe()

recipe.addInput( shape = train_data.shape , name = 'Input' )

recipe.setLayerDefaults( type = tb.layers.conv2d ,
                         activation = tb.activs.relu ,
                         strides = 2 , ksize = 3 )

recipe.addLayer( name = 'conv_1' , out_channels = channels[0] )
recipe.addLayer( name = 'conv_2' , out_channels = channels[1] )
recipe.addLayer( name = 'conv_3' , out_channels = channels[2] )

recipe.addLayer( name = 'Latent' , out_channels = latent_size ,
                 type = tb.layers.variational , activation = None )
recipe.addLayer( name = 'Decode' , out_channels = channels[2] ,
                 type = tb.layers.fully , out_sides = 'Latent' )

recipe.setLayerDefaults( type = tb.layers.deconv2d )

recipe.addLayer( name = 'deconv_1' , out_channels = channels[1] , out_sides = 'conv_3' )
recipe.addLayer( name = 'deconv_2' , out_channels = channels[0] , out_sides = 'conv_2' )
recipe.addLayer( name = 'deconv_3' , out_channels = 'Input'     , out_sides = 'conv_1' ,
                 activation = tb.activs.sigmoid )

recipe.addLayer( type = tb.layers.flatten , name = 'Output' )

recipe.addOperation( function = tb.ops.mean_variational ,
                     input = [ 'Output' , 'Input' ] , extra = 'Latent' , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_squared_error ,
                     input = [ 'Output' , 'Input' ] , name = 'Eval' )
recipe.addOperation( function = tb.optims.adam , learning_rate = 1e-3 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addPlotter( function = tb.plotters.reconst ,
                   name = 'Plotter' , dir = 'figures/cvae2' )

recipe.addSummaryScalar( input = 'Cost' )
recipe.addSummary( name = 'Summary' )
recipe.addWriter( name = 'Writer' , dir = 'logs/cvae2' )

recipe.initialize()
recipe.printNodes()

recipe.train(
        train_data = train_data ,
        test_data = test_data , test_length = 20 ,
        optimizer = 'Optimizer' , summary = 'Summary' , writer = 'Writer' ,
        eval_function = [ 'Cost' , 'Eval' ] , eval_freq = 1 ,
        plot_function = 'Plotter', plot_freq = 5 ,
        size_batch = 1000 , num_epochs = 500 )
