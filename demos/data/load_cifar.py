
import numpy as np

def unpickle( file ):

    import pickle

    fo = open( file , 'rb' )
    dict = pickle.load( fo , encoding = 'latin1' )
    fo.close()

    return dict

batch1 = unpickle( './cifar-10-batches-py/data_batch_1' )
data1 , label1 = np.asarray( batch1['data'] ) , np.asarray( batch1['labels'] )

batch2 = unpickle( './cifar-10-batches-py/data_batch_2' )
data2 , label2 = np.asarray( batch2['data'] ) , np.asarray( batch2['labels'] )

batch3 = unpickle( './cifar-10-batches-py/data_batch_3' )
data3 , label3 = np.asarray( batch3['data'] ) , np.asarray( batch3['labels'] )

batch4 = unpickle( './cifar-10-batches-py/data_batch_4' )
data4 , label4 = np.asarray( batch4['data'] ) , np.asarray( batch4['labels'] )

batch5 = unpickle( './cifar-10-batches-py/data_batch_5' )
data5 , label5 = np.asarray( batch5['data'] ) , np.asarray( batch5['labels'] )

batch6 = unpickle( './cifar-10-batches-py/test_batch' )
data6 , label6 = np.asarray( batch6['data'] ) , np.asarray( batch6['labels'] )

data_train = np.concatenate( [ data1 , data2 , data3 , data4 , data5 ] )
data_test = np.concatenate( [ data6 ] )

label_train = np.concatenate( [ label1 , label2 , label3 , label4 , label5 ] )
label_test = np.concatenate( [ label6 ] )

print( data_train.shape )
print( label_train.shape )
print( data_test.shape )
print( label_test.shape )

label_train_hot = np.zeros( [ label_train.shape[0] , 10 ] )
for i in range( label_train.shape[0] ):
    n = label_train[i]
    label_train_hot[i,n] = 1
label_train = label_train_hot

label_test_hot = np.zeros( [ label_test.shape[0] , 10 ] )
for i in range( label_test.shape[0] ):
    n = label_test[i]
    label_test_hot[i,n] = 1
label_test = label_test_hot

np.save( 'cifar_train_images.npy' , data_train )
np.save( 'cifar_train_labels.npy' , label_train )
np.save( 'cifar_test_images.npy' , data_test )
np.save( 'cifar_test_labels.npy' , label_test )

