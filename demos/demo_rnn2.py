
import sys
sys.path.append( '..' )
import tensorblock as tb

train_data   = tb.aux.load_numpy( './data/mnist_train_images' )
train_labels = tb.aux.load_numpy( './data/mnist_train_labels' )
test_data    = tb.aux.load_numpy( './data/mnist_test_images'  )
test_labels  = tb.aux.load_numpy( './data/mnist_test_labels'  )

recipe = tb.recipe()

recipe.addInput( shape = train_data.shape , name = 'Input' , out_sides = [ 28 , 28 ] )
recipe.addInput( shape = train_labels.shape , name = 'Label' )

recipe.addLayer( type = tb.layers.rnn , input = 'Input' ,
                 out_channels = 200 , name = 'LSTM' )

recipe.addLayer( type = tb.layers.fully , input = 'LSTM' , activation = None ,
                 out_channels = 'Label' , name = 'Output' , weight_seed = 1.0 , bias_seed = 1.0 )

recipe.addOperation( function = tb.ops.mean_soft_cross_logit ,
                     input = [ 'Output' , 'Label' ] , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_equal_argmax ,
                     input = [ 'Output' , 'Label' ] , name = 'Evaluation' )
recipe.addOperation( function = tb.optims.adam , learning_rate = 1e-3 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addWriter( name = 'Writer' , dir = 'logs/rnn2' )

recipe.initialize()
#recipe.printNodes()
#recipe.printCollection()

recipe.train(
        train_data = train_data , train_labels = train_labels ,
        test_data = test_data , test_labels = test_labels , test_length = 1000 ,
        optimizer = 'Optimizer' ,
        eval_function = [ 'Cost' , 'Evaluation' ] , eval_freq = 1 ,
        size_batch = 100 , num_epochs = 500 )

