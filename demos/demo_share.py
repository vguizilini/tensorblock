
import sys
sys.path.append( '..' )
import tensorblock as tb
import numpy as np

recipe = tb.recipe()

block1 = recipe.addBlock( 'Block1' )
block1.addInput( shape = [ None , 6 ] , name = 'Input' )
block1.addLayer( input = 'Input' , out_channels = 6 ,
                 type = tb.layers.fully , name = 'Layer1')
block1.addLayer( input = 'Layer1' , out_channels = 6 ,
                 type = tb.layers.fully , name = 'Layer2' ,
                 weight_share = 'W_Layer1' )


block2 = recipe.copyBlock( src = 'Block1' , dst = 'Block2' )
block2.addLayer( input = '/Block1/Layer2' , name = 'Layer3' , out_channels = 6 ,
                 type = tb.layers.fully ,  bias_type = None ,
                 weight_copy = '../Block1/W_Layer2' )

block2.addLayer( input = 'Input' , share = '../Block2/Layer1' , name = 'Layer4' )

recipe.addWriter( dir = 'logs/share' )

recipe.initialize()

print( '#########################################' )
print( recipe.eval( 'Block1/W_Layer1' ) )
print( recipe.eval( 'Block1/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer1' ) )
print( recipe.eval( 'Block2/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer3' ) )

mat = np.ones( ( 6 , 6 ) )
recipe.assign( 'Block1/W_Layer1' , mat )

print( '#########################################' )
print( recipe.eval( 'Block1/W_Layer1' ) )
print( recipe.eval( 'Block1/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer1' ) )
print( recipe.eval( 'Block2/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer3' ) )

mat = 4 * np.ones( ( 6 , 6 ) )
recipe.assign( 'Block2/W_Layer1' , mat )

print( '#########################################' )
print( recipe.eval( 'Block1/W_Layer1' ) )
print( recipe.eval( 'Block1/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer1' ) )
print( recipe.eval( 'Block2/W_Layer2' ) )
print( recipe.eval( 'Block2/W_Layer3' ) )
