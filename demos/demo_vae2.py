
import sys
sys.path.append( '..' )
import tensorblock as tb

train_data = tb.aux.load_numpy( './data/mnist_train_images' )
test_data  = tb.aux.load_numpy( './data/mnist_test_images'  )

recipe = tb.recipe()

recipe.addInput( shape = train_data.shape , name = 'Input' )

channels = [ 32 , 64 , 128 ]
latent_size = 50

recipe.setLayerDefaults( type = tb.layers.fully ,
                         activation = tb.activs.softplus )

recipe.addLayer( name = 'encode_1' , out_channels = channels[0] )
recipe.addLayer( name = 'encode_2' , out_channels = channels[1] )
recipe.addLayer( name = 'encode_3' , out_channels = channels[2] )

recipe.addLayer( name = 'Latent' , out_channels = latent_size ,
                 type = tb.layers.variational , activation = None )

recipe.addLayer( name = 'decode_3' , out_channels = channels[2] )
recipe.addLayer( name = 'decode_2' , out_channels = channels[1] )
recipe.addLayer( name = 'decode_1' , out_channels = channels[0] )

recipe.addLayer( name = 'Output' , out_channels = 'Input' ,
                 activation = tb.activs.sigmoid )

recipe.addOperation( function = tb.ops.mean_variational ,
                     input = [ 'Output' , 'Input' ] , extra = 'Latent' , name = 'Cost' )
recipe.addOperation( function = tb.ops.mean_squared_error ,
                     input = [ 'Output' , 'Input' ] , name = 'Eval' )
recipe.addOperation( function = tb.optims.adam , learning_rate = 1e-3 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addPlotter( function = tb.plotters.reconst ,
                   name = 'Plotter' , dir = 'figures/vae2' )

recipe.addSummaryScalar( input = 'Cost' )
recipe.addSummary( name = 'Summary' )
recipe.addWriter( name = 'Writer' , dir = 'logs/vae2' )

recipe.initialize()
recipe.printNodes()

recipe.train(
        train_data = train_data ,
        test_data = test_data , test_length = 20 ,
        optimizer = 'Optimizer' , summary = 'Summary' , writer = 'Writer' ,
        eval_function = [ 'Cost' , 'Eval' ] , eval_freq = 1 ,
        plot_function = 'Plotter', plot_freq = 5 ,
        size_batch = 100 , num_epochs = 500 )
