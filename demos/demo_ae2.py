
import sys
sys.path.append( '..' )
import tensorblock as tb

train_data = tb.aux.load_numpy( './data/mnist_train_images' )
test_data  = tb.aux.load_numpy( './data/mnist_test_images'  )

recipe = tb.recipe()

recipe.addInput( shape = train_data.shape , name = 'Input' )

out_channels = [ 32 , 64 , 128 ]
latent_size = 50

recipe.setLayerDefaults( type = tb.layers.fully ,
                         activation = tb.activs.relu )

for n in out_channels:
    recipe.addLayer( out_channels = n )

recipe.addLayer( out_channels = latent_size ,
                 activation = None , name = "Latent" )

for n in reversed( out_channels ):
    recipe.addLayer( out_channels = n )

recipe.addLayer( out_channels = 'Input' , name = "Output" )

recipe.addOperation( function = tb.ops.mean_squared_error ,
                     input = [ 'Output' , 'Input' ] , name = 'Cost' )
recipe.addOperation( function = tb.optims.adam , learning_rate = 1e-3 ,
                     input = 'Cost' , name = 'Optimizer' )

recipe.addPlotter( function = tb.plotters.reconst ,
                   name = 'Plotter' , dir = 'figures/ae2' )

recipe.addSummaryScalar( input = 'Cost' )
recipe.addSummary( name = 'Summary' )
recipe.addWriter( name = 'Writer' , dir = 'logs/ae2' )

recipe.initialize()
recipe.printNodes()

recipe.train(
        train_data = train_data ,
        test_data = test_data , test_length = 20 ,
        optimizer = 'Optimizer' , summary = 'Summary' , writer = 'Writer' ,
        eval_function = 'Cost' , eval_freq = 1 ,
        plot_function = 'Plotter', plot_freq = 5 ,
        size_batch = 1000 , num_epochs = 500 )
