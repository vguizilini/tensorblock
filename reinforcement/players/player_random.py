
from players.player import player

##### PLAYER RANDOM
class player_random( player ):

    ### __INIT__
    def __init__( self ):

        player.__init__( self )

    ### CALCULATE ACTION
    def act( self , obsv ):

        return self.create_random_action()

