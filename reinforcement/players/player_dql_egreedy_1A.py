
from players.player_dql_egreedy import *

##### PLAYER DQL EGREEDY 1A
class player_dql_egreedy_1A( player_dql_egreedy ):

    NUM_FRAMES = 3
    BATCH_SIZE = 100

    LEARNING_RATE = 1e-5
    REWARD_DISCOUNT = 0.99

    START_RANDOM_PROB = 1.00
    FINAL_RANDOM_PROB = 0.05
    NUM_EXPLORATION_EPISODES = 250

    EXPERIENCES_LEN = 100000
    STEPS_BEFORE_TRAIN = 1000

    ### __INIT__
    def __init__( self ):

        player_dql_egreedy.__init__( self )

    ### PROCESS OBSERVATION
    def process( self , obsv ):

        return np.stack( tuple( self.obsv_list[i] for i in range ( self.NUM_FRAMES ) ) , axis = 1 )

    ### PREPARE NETWORK
    def network( self ):

        # Input Placeholder

        self.brain.addInput( shape = [ None , self.obsv_shape[0] , self.NUM_FRAMES ] ,
                             name = 'Observation' )

        # Fully Connected Layers

        self.brain.setLayerDefaults( type = tb.layers.fully ,
                                     activation = tb.activs.relu ,
                                     weight_stddev = 0.01 , bias_stddev = 0.01 )

        self.brain.addLayer( out_channels = 256 , input = 'Observation' )
        self.brain.addLayer( out_channels = 256 )
        self.brain.addLayer( out_channels = self.num_actions  ,
                             activation = None , name = 'Output' )
