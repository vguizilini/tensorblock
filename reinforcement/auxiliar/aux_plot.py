
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

### PLOT STATE
def plot_state( state ):

    n = state.shape[2]

    fig = plt.figure()
    for i in range( n ):
        plt.subplot( 2 , n , i + 1 )
        plt.imshow( state[:,:,i] , cmap = 'gray' )
    plt.show()

### PLOT STATES
def plot_states( prev_state , curr_state ):

    n = prev_state.shape[2]

    fig = plt.figure()
    for i in range( n ):
        plt.subplot( 2 , n , i + 1 )
        plt.imshow( curr_state[:,:,i] , cmap = 'gray' )
        plt.subplot( 2 , n , i + n + 1 )
        plt.imshow( prev_state[:,:,i] , cmap = 'gray' )
    plt.show()
