
import time
from collections import deque

##### SOURCE
class source:

    ### __INIT__
    def __init__( self ):

        self.elapsed_time = 0
        self.avg_rewd = deque()
        self.sum_rewd = 0

        self.timer = time.time()

        return None

    ### DUMMY FUNCTIONS
    def num_actions( self ): return 0
    def map_keys( self , action ): return 0
    def process( self , obsv ): return obsv


    ### VERBOSE OUTPUT
    def verbose( self , episode , rewd , done , avg_length = 10 ):

        self.sum_rewd += rewd

        if done:

            self.avg_rewd.append( self.sum_rewd )
            if len( self.avg_rewd ) > avg_length : self.avg_rewd.popleft()

            now = time.time()
            self.elapsed_time = ( now - self.timer )
            self.timer = now

            print( '*** Episode : %5d | Time : %6.2f s | Rewards : %9.3f | Average : %9.3f |' % \
                   ( episode + 1 , self.elapsed_time , self.sum_rewd ,
                     sum( self.avg_rewd ) / len( self.avg_rewd ) ) , end = '' )
            self.env.info()

            self.sum_rewd = 0

