
import pygame
import pygame.surfarray as surfarray
import numpy as np

class pygame_maze:

    ### START SIMULATION
    def reset( self ):

        pygame.init()

        self.black = [  0  ,  0  ,  0  ]
        self.white = [ 255 , 255 , 255 ]
        self.red   = [ 255 ,  0  ,  0  ]
        self.green = [  0  , 255 ,  0  ]
        self.blue  = [  0  ,  0  , 255 ]

        self.screen_size = [ 600 , 600 ]

        self.image = pygame.image.load( 'sources/maps/map3.png' )
        self.image = pygame.surfarray.pixels3d( self.image )

        self.traj = np.load( 'sources/maps/map3_traj.npy' )
        self.traj_len = len( self.traj )

        self.robot_diam = 10
        self.robot_rad = int( self.robot_diam / 2 )

        self.robot_pos = self.traj[0].copy()
        self.robot_theta = np.arctan2( self.traj[1][1] - self.traj[0][1] ,
                                       self.traj[1][0] - self.traj[0][0] ) * 180.0 / np.pi
        self.robot_vels = [ 5 , 3 ]

        self.angle_min , self.angle_max = - 90 , + 90
        self.angle_res , self.dist_max = 10 , 80
        self.track_point , self.track_reach = 0 , 10

        self.angle_num = int( ( self.angle_max - self.angle_min ) / self.angle_res ) + 1

        self.screen = pygame.display.set_mode( self.screen_size , 0 , 32 )

        map_surf = pygame.surfarray.make_surface( self.image )
        self.map = map_surf.convert()

        robot_surf = pygame.Surface( [ self.robot_diam , self.robot_diam ] )
        pygame.draw.circle( robot_surf , self.green , [ self.robot_rad , self.robot_rad ] , self.robot_rad )

        self.robot = robot_surf.convert()
        self.robot.set_colorkey( self.black )

        return self.draw()

    ### DRAW SCREEN
    def draw( self ):

        self.screen.blit( self.map , ( 0 , 0 ) )

        returns = np.zeros( self.angle_num )

        angle = self.angle_min
        for i in range( self.angle_num ):

            return_pos = [ 0 , 0 ]
            return_angle = ( self.robot_theta + angle ) * np.pi / 180.0

            hit = False
            for j in range( self.dist_max ):

                return_pos[0] = self.robot_pos[0] + j * np.cos( return_angle )
                return_pos[1] = self.robot_pos[1] + j * np.sin( return_angle )

                hit = self.image[ int( return_pos[0] ) , int( return_pos[1] ) , 0 ] == 0
                if hit: break

            returns[i] = self.dist_max if not hit else \
                                        np.sqrt( ( self.robot_pos[0] - return_pos[0] )**2 +
                                                 ( self.robot_pos[1] - return_pos[1] )**2 )

            color = self.red if hit else self.blue
            pygame.draw.line( self.screen , color , self.robot_pos , return_pos , 2 )
            angle += self.angle_res

        self.screen.blit( self.robot , [ self.robot_pos[0] - self.robot_rad ,
                                         self.robot_pos[1] - self.robot_rad ] )

        for i in range( 1 , self.traj_len ):
            pygame.draw.line( self.screen , self.red , self.traj[i-1] , self.traj[i] , 2 )
        self.screen.blit( self.robot , [ self.traj[ self.track_point ][0] - self.robot_rad ,
                                         self.traj[ self.track_point ][1] - self.robot_rad ] )

        pygame.display.update()
        return returns

    ### MOVE ONE STEP
    def step( self , action ):

        # Initialize Reward

        rewd = 0.0

        # Execute Action

        if action == 0 :
            self.robot_pos[0] += self.robot_vels[0] * np.cos( self.robot_theta * np.pi / 180.0 )
            self.robot_pos[1] += self.robot_vels[0] * np.sin( self.robot_theta * np.pi / 180.0 )
        if action == 1 :
            self.robot_theta += self.robot_vels[1]
            self.robot_pos[0] += self.robot_vels[0] * np.cos( self.robot_theta * np.pi / 180.0 )
            self.robot_pos[1] += self.robot_vels[0] * np.sin( self.robot_theta * np.pi / 180.0 )
        if action == 2 :
            self.robot_theta -= self.robot_vels[1]
            self.robot_pos[0] += self.robot_vels[0] * np.cos( self.robot_theta * np.pi / 180.0 )
            self.robot_pos[1] += self.robot_vels[0] * np.sin( self.robot_theta * np.pi / 180.0 )

        # Measure Progress

        min_traj = self.track_point - self.track_reach
        max_traj = self.track_point + self.track_reach

        if min_traj < 0              : min_traj = 0
        if max_traj >= self.traj_len : max_traj = self.traj_len - 1

        traj = self.traj[ min_traj : max_traj ]

        min_dist , idx = 1e12 , 0
        for i , pt in enumerate( traj ):
            dist = np.sum( np.square( pt - self.robot_pos ) )
            if dist < min_dist : min_dist , idx = dist , i
        self.track_point = min_traj + idx

        # Check If Hit

        if self.image[ int( self.robot_pos[0] ) , int( self.robot_pos[1] ) , 0 ] == 0:
            return self.draw() , -1000 , True

        # Check if Finish

        if self.track_point == self.traj_len - 1:
            return self.draw() , +1000 , True

        # No Information

        rewd = float( self.track_point ) / float( self.traj_len ) * 100
        return self.draw() , rewd  , False

